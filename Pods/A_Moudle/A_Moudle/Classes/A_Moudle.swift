//
//  A_Moudle.swift
//  A_Moudle
//
//  Created by trembleCat on 01/24/2022.
//  Copyright (c) 2022 trembleCat. All rights reserved.
//

import Foundation
import MyRouter
import OtherMoudle

//MARK: - 模块A
public class A_Moudle: RouterMoudleProtocol {
    public var moudle: String { "A_Moudle" }
    public var scheme: String { "apps" }
    public var pathDic: [String: String] {
        ["pathA":"A_Controller",
         "pathA_Detail":"A_DetailController"]
    }
    
    public class func registerPages() {
        A_Moudle().registerPages()
    }
}

//MARK: - 演示用途, 可进行二次封装, 单独为该模块进行封装
class A_ModuleNetWorkManager: NetworkManager {
    static let shared = A_ModuleNetWorkManager()
}


