# A_Moudle

[![CI Status](https://img.shields.io/travis/trembleCat/A_Moudle.svg?style=flat)](https://travis-ci.org/trembleCat/A_Moudle)
[![Version](https://img.shields.io/cocoapods/v/A_Moudle.svg?style=flat)](https://cocoapods.org/pods/A_Moudle)
[![License](https://img.shields.io/cocoapods/l/A_Moudle.svg?style=flat)](https://cocoapods.org/pods/A_Moudle)
[![Platform](https://img.shields.io/cocoapods/p/A_Moudle.svg?style=flat)](https://cocoapods.org/pods/A_Moudle)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

A_Moudle is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'A_Moudle'
```

## Author

trembleCat, fa_dou_miao@163.com

## License

A_Moudle is available under the MIT license. See the LICENSE file for more info.
