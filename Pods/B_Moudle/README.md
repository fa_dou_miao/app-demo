# B_Moudle

[![CI Status](https://img.shields.io/travis/trembleCat/B_Moudle.svg?style=flat)](https://travis-ci.org/trembleCat/B_Moudle)
[![Version](https://img.shields.io/cocoapods/v/B_Moudle.svg?style=flat)](https://cocoapods.org/pods/B_Moudle)
[![License](https://img.shields.io/cocoapods/l/B_Moudle.svg?style=flat)](https://cocoapods.org/pods/B_Moudle)
[![Platform](https://img.shields.io/cocoapods/p/B_Moudle.svg?style=flat)](https://cocoapods.org/pods/B_Moudle)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

B_Moudle is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'B_Moudle'
```

## Author

trembleCat, fa_dou_miao@163.com

## License

B_Moudle is available under the MIT license. See the LICENSE file for more info.
