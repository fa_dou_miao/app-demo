//
//  B_Moudle.swift
//  B_Moudle
//
//  Created by 发抖喵 on 2022/1/27.
//

import Foundation
import MyRouter
import OtherMoudle 

public class B_Moudle: RouterMoudleProtocol {
    public var moudle: String { "B_Moudle" }
    
    public var scheme: String { "bpps" }
    
    public var pathDic: [String : String] {
        ["path/b":"B_Controller",
         "path/b/detail":"B_DetailController"]
    }
    
    public class func registerPages() {
        B_Moudle().registerPages()
    }
}

//MARK: - 演示用途, 可进行二次封装, 单独为该模块进行封装
class B_ModuleNetWorkManager: NetworkManager {
    static let shared = B_ModuleNetWorkManager()
}
