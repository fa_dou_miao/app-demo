//
//  B_Controller.swift
//  B_Moudle
//
//  Created by 发抖喵 on 2022/1/27.
//

//MARK: - 进入该页面不需要传入参数的演示
public class B_Controller: UIViewController {
    
    private lazy var titleLabel = UILabel()
    private lazy var jumpButton = UIButton(type: .system)
    private lazy var jumpErrrorButton = UIButton(type: .system)
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "B_Controller"
        self.view.backgroundColor = .white
        
        setupUI()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        loadData()
    }
}

//MARK: - UI
extension B_Controller {
    private func setupUI() {
        self.view.addSubview(titleLabel)
        self.view.addSubview(jumpButton)
        self.view.addSubview(jumpErrrorButton)
        
        titleLabel.text = "B_Controller"
        titleLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        
        jumpButton.setTitle("点击 present A_Moudle.A_DetailController", for: .normal)
        jumpButton.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
        jumpButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom).offset(20)
        }
        
        jumpErrrorButton.setTitle("点击跳转一个错误的 url", for: .normal)
        jumpErrrorButton.addTarget(self, action: #selector(clickErrorButton), for: .touchUpInside)
        jumpErrrorButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(jumpButton.snp.bottom).offset(20)
        }
    }
}

//MARK: - Action
extension B_Controller {
    /**
     点击跳转按钮
     */
    @objc private func clickButton() {
        let detailParameters: [String: Any] = ["id": "0000", "name": "name0000", "image": UIImage()]
        self.presentRouterControllerWithUrl("apps://pathA_Detail", parameters: detailParameters, animated: true) { parameters in
            // 页面参数回调
            print("==========================================")
            print("当前页面收到 apps://pathA_Detail 的回调")
            print(parameters)
            print("==========================================")
        }
    }
    
    /**
     点击跳转错误页面按钮
     */
    @objc private func clickErrorButton() {
        self.pushRouterControllerWithUrl("apps://-123-213")
        print("点击了跳转错误页面的按钮，如果你对错误通知进行了监听，应该是可以收到错误通知的")
    }
}

//MARK: - Network
extension B_Controller {
    private func loadData() {
        B_ModuleNetWorkManager.shared.request("请求网络数据")
        print("B_Controller 请求网络数据")
    }
}

