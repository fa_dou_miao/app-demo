# OtherMoudle

[![CI Status](https://img.shields.io/travis/trembleCat/OtherMoudle.svg?style=flat)](https://travis-ci.org/trembleCat/OtherMoudle)
[![Version](https://img.shields.io/cocoapods/v/OtherMoudle.svg?style=flat)](https://cocoapods.org/pods/OtherMoudle)
[![License](https://img.shields.io/cocoapods/l/OtherMoudle.svg?style=flat)](https://cocoapods.org/pods/OtherMoudle)
[![Platform](https://img.shields.io/cocoapods/p/OtherMoudle.svg?style=flat)](https://cocoapods.org/pods/OtherMoudle)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

OtherMoudle is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'OtherMoudle'
```

## Author

trembleCat, fa_dou_miao@163.com

## License

OtherMoudle is available under the MIT license. See the LICENSE file for more info.
