//
//  OtherMoudle.swift
//  OtherMoudle
//
//  Created by 发抖喵 on 2022/1/30.
//
//  这里导入 SnapKit 与 Kingfisher，虽不做任何操作，
//  实际上是因为两库都是对系统类的扩展，因为是扩展，所以在其他模块中可以直接使用snapkit和kingfish
//  当然对 Kingfish 这种完全可以再进行一次封装

//  而其他库例如 AFN, YYText 这种需要使用自定义类的库，则需要对使用的类进行封装继承才可以使用

import Foundation
import SnapKit
import Kingfisher
import YYText
import Alamofire

/// 简单演示一下就都写同一个文件了，实际开发中，需要对每个功能进行封装

/// 对 Pod OC 库的使用
open class AppLabel: YYLabel { }
open class AppTextView: YYTextView { }

/// 演示
open class NetworkManager: NSObject {
    
    open func request(_ url: URLConvertible) {
        AF.request(url)
        // 演示代码
    }
}

/// 对自创建的 OC 类的使用
open class TestClass: NSObject {
    public var test = OCTest()
}
