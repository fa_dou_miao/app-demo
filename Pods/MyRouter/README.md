# MyRouter

[![CI Status](https://img.shields.io/travis/trembleCat/MyRouter.svg?style=flat)](https://travis-ci.org/trembleCat/MyRouter)
[![Version](https://img.shields.io/cocoapods/v/MyRouter.svg?style=flat)](https://cocoapods.org/pods/MyRouter)
[![License](https://img.shields.io/cocoapods/l/MyRouter.svg?style=flat)](https://cocoapods.org/pods/MyRouter)
[![Platform](https://img.shields.io/cocoapods/p/MyRouter.svg?style=flat)](https://cocoapods.org/pods/MyRouter)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MyRouter is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MyRouter'
```

## Author

trembleCat, fa_dou_miao@163.com

## License

MyRouter is available under the MIT license. See the LICENSE file for more info.
