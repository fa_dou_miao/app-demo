//
//  ErrorViewController.swift
//  AppDemo
//
//  Created by 发抖喵 on 2022/2/4.
//

import UIKit

class ErrorViewController: UIViewController {
    
    private lazy var errorButton = UIButton(type: .system)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white

        setupUI()
    }
}

//MARK: - UI
extension ErrorViewController {
    private func setupUI() {
        self.view.addSubview(errorButton)
        
        errorButton.setTitle("错误页面，点击返回上一页", for: .normal)
        errorButton.addTarget(self, action: #selector(clickErrorButton), for: .touchUpInside)
        errorButton.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}

//MARK: - Action
extension ErrorViewController {
    @objc private func clickErrorButton() {
        self.dismissRouterController(animated: true)
    }
}
