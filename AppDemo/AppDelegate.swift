//
//  AppDelegate.swift
//  AppDemo
//
//  Created by 发抖喵 on 2022/1/27.
//

import UIKit
import MyRouter
import A_Moudle
import B_Moudle

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // register
        A_Moudle.registerPages()
        B_Moudle.registerPages()
        
        // 监听路由错误通知
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(routerErrorNotification),
                                               name: .init(MyRouter.routerErrorNotificaiton),
                                               object: nil)
        
        // rootVC
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootController()
        window?.makeKeyAndVisible()
        
        return true
    }
    
    /**
     演示用
     */
    func rootController() -> UIViewController {
        if let a_VC = MyRouter.shared.viewController("apps://pathA"),
        let b_VC = MyRouter.shared.viewController("bpps://path/b") {
            
            let nav_AC = UINavigationController(rootViewController: a_VC)
            nav_AC.title = "A_Controller"
            nav_AC.tabBarItem.image = UIImage(named: "Normal")
            nav_AC.tabBarItem.setTitleTextAttributes([.foregroundColor: UIColor.orange], for: .selected)
            nav_AC.tabBarItem.setTitleTextAttributes([.foregroundColor: UIColor.gray], for: .normal)
            
            let nav_BC = UINavigationController(rootViewController: b_VC)
            nav_BC.title = "B_Controller"
            nav_BC.tabBarItem.image = UIImage(named: "Normal")
            nav_BC.tabBarItem.setTitleTextAttributes([.foregroundColor: UIColor.orange], for: .selected)
            nav_BC.tabBarItem.setTitleTextAttributes([.foregroundColor: UIColor.gray], for: .normal)
            
            let tabVC = UITabBarController()
            tabVC.addChild(nav_AC)
            tabVC.addChild(nav_BC)
            
            return tabVC
        }
        
        return UIViewController()
    }
    
    @objc func routerErrorNotification() {
        print("收到错误信息, 弹出一个错误页面")
        window?.rootViewController?.present(ErrorViewController(), animated: true, completion: nil)
    }
}

